<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Dog Content</title>
    <link rel="shortcut icon" href="">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
    <style>body{padding-top:50px;}.starter-template{padding:40px 15px;text-align:center;}</style>

</head>

<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Le Dog Form</a>
            </div>
        </div>
    </nav>

   
        <form:form commandName="dogFormGreeting">
        	<div class="form-group">
        		<form:label path="name" for="catName">Cat Name</form:label>
        		<form:input type="text" class="form-control" id="catNameInput" path="name" placeholder="Enter Name"/>
        	</div>
        	<div class="form-group">
        		<form:label path= "age" for="catAge">Cat Age</form:label>
        		<form:input type="number" class="form-control" id="catAgeInput" path="age" placeholder="Enter Age"/>
        	</div>
        	<div class="form-group">
        		<form:label path="breed" for="catBreed">Cat Breed</form:label>
        		<form:input type="text" class="form-control" id="catBreedInput" path="breed" placeholder="Enter Breed"/>
        	</div>
        	<div class="form-group">
        		<form:label path = "content" for="catComment">Any Comments?</form:label>
        		<form:input type="text" class="form-control" id="catCommentInput" path="content" placeholder="Enter them here"/>
        	</div>
        	<form:button type="submit" class = "btn btn-default" value="Submit">Submit</form:button>
        </form:form>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
</body>
</html>