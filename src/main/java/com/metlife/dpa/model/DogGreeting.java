package com.metlife.dpa.model;


public class DogGreeting {

    private String content;
    private String name;
    private int age;
    private String breed;

    public String getContent() {
        return content;
    }
    
    public String getName() {
        return name;
    }
    
    public int getAge() {
        return age;
    }
    
    public String getBreed() {
        return breed;
    }

    public void setContent(String content) {
        this.content = content;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setAge(int age) {
        this.age = age;
    }
    
    public void setBreed(String breed) {
        this.breed = breed;
    }

}
